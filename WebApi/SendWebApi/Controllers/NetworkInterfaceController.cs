﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace SendWebApi.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    /// <summary>
    /// Контроллер для получение информации о сетевых интерфейсах.
    /// </summary>
    public class NetworkInterfaceController : ControllerBase
    {
        /// <summary>
        /// Характеристики сетевого интерфейса.
        /// </summary>
        public struct MyNetworkInterface
        {
            // Id интерфейса.
            public string Id { get; set; }
            // Дескриптор интерфейса.
            public string Description { get; set; }
            // Тип интерфейса.
            public string InterfaceType { get; set; }
            // Физический адрес интерфейса.
            public string PhysicalAddress { get; set; }
            //Статус операции интерфейса
            public string OperationalStatus { get; set; }
            // IP версия интерфейса.
            public string IPVersion  { get; set; }
            // DNSSuffix интерфейса.
            public string DNSSuffix { get; set; }
            // MTU интерфейса.
            public string MTU { get; set; }
            // Статус DNS интерфейса.
            public string DNSEnabled { get; set; }
            // Конфигурация DNS интерфейса.
            public string DynamicallyConfiguredDNS { get; set; }
            // ReceiveOnly интерфейса.
            public string ReceiveOnly { get; set; }
            // Multicast интерфейса.
            public string Multicast { get; set; }
        }
        /// <summary>
        /// Информация о трафики интерфейса.
        /// </summary>
        public struct InfoTraffic
        {
            // Дескриптор интерфейса.
            public string Description { get; set; }
            // Скорость интерфейса.
            public string Speed { get; set; }
            // Число байтов, полученных интерфейсом.
            public string BytesReceived { get; set; }
            // Число байтов, отправленных интерфейсом.
            public string BytesSent { get; set; }
            // Число входящих пакетов, которые были удалены
            public string IncomingPacketsDiscarded { get; set; }
            // Число входящих пакетов с ошибками.
            public string IncomingPacketsWithErrors { get; set; }
            // Число входящих пакетов с неизвестным протоколом.
            public string IncomingUnknownProtocolPackets { get; set; }
            // Число неодноадресных пакетов, полученных интерфейсом.
            public string NonUnicastPacketsReceived { get; set; }
            // Число неодноадресных пакетов, отправленных интерфейсом.
            public string NonUnicastPacketsSent { get; set; }
            // Число исходящих пакетов, которые были удалены.
            public string OutgoingPacketsDiscarded { get; set; }
            // Число исходящих пакетов с ошибками.
            public string OutgoingPacketsWithErrors { get; set; }
            // Длину очереди вывода.
            public string OutputQueueLength { get; set; }
            //  Число одноадресных пакетов, полученных интерфейсом.
            public string UnicastPacketsReceived { get; set; }
            // Число одноадресных пакетов, отправленных интерфейсом.
            public string UnicastPacketsSent { get; set; }
        }
        /// <summary>
        /// Получение информации о сетевых интерфейсах.
        /// </summary>
        [HttpGet]
        public MyNetworkInterface[] GetInfoNetworkInterface()
        {
            //Список всех интерфейсов с характеристиками.
            List<MyNetworkInterface> list = new List<MyNetworkInterface>();
            //Все сетевые интерфейсы.
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            if (nics == null || nics.Length < 1)
            {
                Console.WriteLine("  No network interfaces found.");
            }
            // Id интерфейса.
            int id_inteface = 0;
            foreach (NetworkInterface adapter in nics)
            {
                //Свойства интерфейса.
                IPInterfaceProperties properties = adapter.GetIPProperties();
                // Версия IP.
                string versions = "";

                if (adapter.Supports(NetworkInterfaceComponent.IPv4))
                {
                    versions = "IPv4";
                }
                if (adapter.Supports(NetworkInterfaceComponent.IPv6))
                {
                    if (versions.Length > 0)
                    {
                        versions += " ";
                    }
                    versions += "IPv6";
                }
                
                // Значение MTU
                string MTUValue = "";
                
                if (adapter.Supports(NetworkInterfaceComponent.IPv4))
                {
                    IPv4InterfaceProperties ipv4 = properties.GetIPv4Properties();
                    MTUValue = Convert.ToString(ipv4.Mtu);
                }

                list.Add(new MyNetworkInterface 
                { 
                    Id = Convert.ToString(id_inteface),
                    Description = adapter.Description,
                    InterfaceType = Convert.ToString(adapter.NetworkInterfaceType),
                    PhysicalAddress = adapter.GetPhysicalAddress().ToString(),
                    OperationalStatus = Convert.ToString(adapter.OperationalStatus),
                    IPVersion = versions,
                    DNSSuffix = properties.DnsSuffix,
                    MTU = Convert.ToString(MTUValue),
                    DNSEnabled = Convert.ToString(properties.IsDnsEnabled),
                    DynamicallyConfiguredDNS = Convert.ToString(properties.IsDynamicDnsEnabled),
                    ReceiveOnly = Convert.ToString(adapter.IsReceiveOnly),
                    Multicast = Convert.ToString(adapter.SupportsMulticast)
                });
                id_inteface++;
            }
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            Console.WriteLine("====Запрос GetInfoNetworkInterface====");
            Console.WriteLine("Ответ сервера:");
            // Ответ сервера для консоли.
            string answer = JsonSerializer.Serialize<List<MyNetworkInterface>>(list,options);
            answer = Regex.Unescape(answer);
            Console.WriteLine(answer);
            

            return list.ToArray();
        }

        [HttpGet("{id}")]
        public InfoTraffic[] GetInfoNetworkTraffic(int id)
        {
            //Информация о трафике интерфейса.
            List<InfoTraffic> list = new List<InfoTraffic>();
            //Все сетевые интерфейсы.
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            // Cтатистикa сетевого интерфейса.
            IPv4InterfaceStatistics info_traffic = nics[id].GetIPv4Statistics();
            list.Add( new InfoTraffic
            {
                Description = nics[id].Description,
                Speed = Convert.ToString(nics[id].Speed),
                BytesReceived = Convert.ToString(info_traffic.BytesReceived),
                BytesSent = Convert.ToString(info_traffic.BytesSent),
                IncomingPacketsDiscarded = Convert.ToString(info_traffic.IncomingPacketsDiscarded),
                IncomingPacketsWithErrors = Convert.ToString(info_traffic.IncomingPacketsWithErrors),
                IncomingUnknownProtocolPackets = Convert.ToString(info_traffic.IncomingUnknownProtocolPackets),
                NonUnicastPacketsReceived = Convert.ToString(info_traffic.NonUnicastPacketsReceived),
                NonUnicastPacketsSent = Convert.ToString(info_traffic.NonUnicastPacketsSent),
                OutgoingPacketsDiscarded = Convert.ToString(info_traffic.OutgoingPacketsDiscarded),
                OutgoingPacketsWithErrors = Convert.ToString(info_traffic.OutgoingPacketsWithErrors),
                OutputQueueLength = Convert.ToString(info_traffic.OutputQueueLength),
                UnicastPacketsReceived = Convert.ToString(info_traffic.UnicastPacketsReceived),
                UnicastPacketsSent = Convert.ToString(info_traffic.UnicastPacketsSent),

            }
                );

            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            Console.WriteLine("====Запрос GetInfoNetworkTraffic====");
            Console.WriteLine("Ответ сервера:");
            // Ответ сервера для консоли.
            string answer = JsonSerializer.Serialize<List<InfoTraffic>>(list, options);
            answer = Regex.Unescape(answer);
            Console.WriteLine(answer);
            return list.ToArray();
        }
    }
}
